#!/bin/bash

export WPDESK_PLUGIN_SLUG=woocommerce-products-exporter
export WPDESK_PLUGIN_TITLE="WooCommerce Products Exporter"

export WOOTESTS_IP=${WOOTESTS_IP:wootests}

sh ./vendor/wpdesk/wp-codeception/scripts/common_bootstrap.sh
