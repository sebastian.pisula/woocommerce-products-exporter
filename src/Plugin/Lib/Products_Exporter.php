<?php
/**
 * Products Exporter class.
 *
 * @package WPDesk\WooCommerceProductsExporter\Lib
 */

namespace WPDesk\WooCommerceProductsExporter\Lib;

use WC_Product;
use WC_Product_Variable;

/**
 * Class Exporter
 *
 * @package WPDesk\WooCommerceProductsExporter\Lib
 */
class Products_Exporter {
	/**
	 * Returns data to export.
	 *
	 * @return array
	 */
	public function get_data() {
		$products = [];

		foreach ( $this->get_products() as $product ) {
			if ( 'variable' === $product->get_type() ) {
				foreach ( $product->get_children() as $variation_id ) {
					$products[] = $this->prepare_product_row( (int) $variation_id );
				}
			} else {
				$products[] = $this->prepare_product_row( $product->get_id() );
			}
		}

		return array_filter( $products );
	}

	/**
	 * Get products.
	 *
	 * @return WC_Product[]|WC_Product_Variable[]
	 */
	private function get_products() {
		return wc_get_products( [
			'status'  => 'publish',
			'orderby' => 'name',
			'order'   => 'ASC',
			'limit'   => - 1,
		] );
	}

	/**
	 * Get prepared CSV row.
	 *
	 * @param int $id
	 *
	 * @return array|null
	 */
	private function prepare_product_row( int $id ) {
		$product = wc_get_product( $id );

		if ( ! $product ) {
			return null;
		}

		return [
			'name'          => $product->get_name(),
			'sku'           => $product->get_sku(),
			'categories'    => implode( ', ', wp_get_post_terms( $product->get_id(), 'product_cat', [ 'fields' => 'names' ] ) ),
			'price'         => (float) $product->get_price(),
			'regular_price' => (float) $product->get_regular_price(),
		];
	}
}