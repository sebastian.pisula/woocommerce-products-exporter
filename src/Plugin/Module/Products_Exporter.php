<?php
/**
 * Module of products exporter class.
 *
 * @package WPDesk\WooCommerceProductsExporter
 */

namespace WPDesk\WooCommerceProductsExporter\Module;

use League\Csv\Writer;

/**
 * Class Products_Exporter
 *
 * @package WPDesk\WooCommerceProductsExporter\Module
 */
class Products_Exporter {
	/**
	 * Register module hooks.
	 */
	public function add_hooks() {
		add_action( 'admin_menu', [ $this, 'add_menu_element' ] );
		add_action( 'admin_post_ic-product-exporter', [ $this, 'process_export' ] );
	}

	/**
	 * Add admin menu new item
	 */
	public function add_menu_element() {
		$url = wp_nonce_url( add_query_arg( 'action', 'ic-product-exporter', admin_url( 'admin-post.php' ) ), 'ic-product-exporter' );

		add_submenu_page( 'edit.php?post_type=product', __( 'Products Exporter', 'woocommerce-products-exporter' ), __( 'Export Products', 'woocommerce-products-exporter' ), 'manage_options', $url );
	}

	/**
	 * Run Export action
	 */
	public function process_export() {
		if ( ! current_user_can( 'manage_options' ) ) {
			wp_die( __( 'Sorry, you are not allowed to do that.', 'woocommerce-products-exporter' ) );
		}

		check_admin_referer( 'ic-product-exporter' );

		$exporter_data = new \WPDesk\WooCommerceProductsExporter\Lib\Products_Exporter();

		$csv = Writer::createFromString( '' );
		$csv->setDelimiter( ';' );
		$csv->setInputEncoding( 'UTF-8' );

		$csv->insertOne( [
			__( 'Name', 'woocommerce-products-exporter' ),
			__( 'SKU', 'woocommerce-products-exporter' ),
			__( 'Categories', 'woocommerce-products-exporter' ),
			__( 'Price', 'woocommerce-products-exporter' ),
			__( 'Regular Price', 'woocommerce-products-exporter' ),
		] );

		$csv->insertAll( $exporter_data->get_data() );
		$csv->output( 'products.csv' );
		die();
	}
}